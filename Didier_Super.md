
# Didier Super

Olivier Haudegond, dit **Didier Super**, est un comédien, humoriste, chanteur et musicien français.

Didier Super apparaît dans le monde médiatique en 2004, avec un CD aux textes corrosifs. Après plusieurs années de concerts, il met depuis 2008 l'accent sur le côté « comique » de sa carrière, avec un one-man-show, une parodie de comédie musicale, et une bande dessinée sur son parcours. 

## Sommaire
### Biographie
#### Jeunesse
#### Carrière musicale
#### Carrière théâtrale
### Réception
### Spectacles
### Bande dessinée
### Discographie
#### Participations
### Notes et références
### Liens externes
