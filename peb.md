# PEB
Pierre-Emmanuel Barré, né en 1984 à Quimperlé, dans le Finistère (France), est un humoriste français, chroniqueur de radio et de télévision.

Adepte de l'humour noir, il est présent sur scène avec sa comédie Full Metal Molière (2007) et son seul-en-scène Pierre-Emmanuel Barré est un sale con (2011). Entre 2012 et 2017, il tient une chronique hebdomadaire sur France Inter dans On va tous y passer puis dans La Bande originale. De 2013 à 2015, il participe à La Nouvelle Édition sur Canal+ avant de rejoindre Folie passagère sur France 2. 